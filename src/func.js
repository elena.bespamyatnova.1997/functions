const getSum = (str1, str2) => {

    let pattern = new RegExp('^[0-9]+$');

    if (!str1) {
        str1 = '0';
    }
    if (!str2) {
        str2 = '0';
    }

    if (Array.isArray(str1) || Array.isArray(str2) || typeof (str1) == 'object' || typeof (str2) == 'object'
        || !pattern.test(str1) || !pattern.test(str2) || typeof (str1) == 'number' || typeof (str2) == 'number') {
        return false;
    }

    return String(Number(str1) + Number(str2));

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let countComments = 0;
    let countPosts = 0;
    for (const i in listOfPosts) {

        if (listOfPosts[i].author == authorName) {
            if (listOfPosts[i].post) {

                countPosts++;
            }


        }
        if (listOfPosts[i].comments) {
            for (const key in listOfPosts[i].comments) {
                if (listOfPosts[i].comments[key].author == authorName) {
                    countComments++;


                }
            }
        }

    }
    return 'Post:' + countPosts + ',comments:' + countComments;
};

const tickets = (people) => {

    let balance = 0;

    for (const i in people) {

        balance += people[i];

        if (people[i] > 25) {
            if (i == '0') {
                return 'NO';
            }

            if (balance - 25 < 0) {
                return 'NO';
            }
            if (balance - people[i] < people[i] - 25) {
                balance = balance + 25 - people[i];
                return 'NO'

            }
        }
        balance = balance + 25 - people[i];
    }
    return 'YES';

};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };

